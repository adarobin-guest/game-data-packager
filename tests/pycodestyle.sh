#!/bin/sh
# Copyright © 2022-2023 Simon McVittie
# Copyright © 2018-2023 Collabora Ltd.
# SPDX-License-Identifier: GPL-2.0-or-later

echo "TAP version 13"

if command -v pycodestyle >/dev/null; then
    :
else
    echo "1..0 # SKIP pycodestyle not found"
    exit 0
fi

cd "$GDP_SRCDIR"

export LC_ALL=C.UTF-8
n=0
failed=0

for file in \
    game_data_packager/*.py \
    game_data_packager/*/*.py \
    runtime/*.py \
    tests/*.py \
    tools/*.py \
; do
    n=$(( n + 1 ))
    if pycodestyle "$file" >&2; then
        echo "ok $n - $file"
    elif [ -n "${LINT_WARNINGS_ARE_ERRORS-}" ]; then
        echo "not ok $n - $file"
        failed=1
    else
        echo "not ok $n - $file # TODO warnings from pycodestyle"
    fi
done

echo "1..$n"
exit "$failed"
