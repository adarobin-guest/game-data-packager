# Copyright 2020 Simon McVittie
#
# SPDX-License-Identifier: FSFAP

import unittest

try:
    from tap.runner import TAPTestRunner
except ImportError:
    TAPTestRunner = None  # type: ignore


def main() -> None:
    if TAPTestRunner is not None:
        runner = TAPTestRunner()
        runner.set_stream(True)
        unittest.main(testRunner=runner)
    else:
        # You thought pycotap was a minimal TAP implementation?
        print('TAP version 13')
        print('1..1')
        program = unittest.main(exit=False, verbosity=2)
        if program.result.wasSuccessful():
            print(
                'ok 1 - %r (tap module not available)'
                % program.result
            )
        else:
            print(
                'not ok 1 - %r (tap module not available)'
                % program.result
            )
            raise SystemExit(1)
