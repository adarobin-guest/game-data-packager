#!/bin/sh
# Copyright © 2022-2023 Simon McVittie
# Copyright © 2018-2023 Collabora Ltd.
# SPDX-License-Identifier: GPL-2.0-or-later

echo "TAP version 13"

if command -v mypy >/dev/null; then
    :
else
    echo "1..0 # SKIP mypy not found"
    exit 0
fi

cd "$GDP_SRCDIR"
export MYPYPATH="$(pwd)"

export LC_ALL=C.UTF-8
n=0
failed=0

for dir in \
    game_data_packager \
    runtime \
    tests \
    tools \
; do
    n=$(( n + 1 ))
    # TODO: --strict
    if mypy \
        --python-executable="${PYTHON:=python3}" \
        --follow-imports=skip \
        --ignore-missing-imports \
        "$dir" >&2
    then
        echo "ok $n - $dir"
    elif [ -n "${LINT_WARNINGS_ARE_ERRORS-}" ]; then
        echo "not ok $n - $dir"
        failed=1
    else
        echo "not ok $n - $dir # TODO warnings from mypy"
    fi
done

echo "1..$n"
exit "$failed"
