#!/usr/bin/python3
# encoding=utf-8
#
# Copyright © 2016 Simon McVittie <smcv@debian.org>
# SPDX-License-Identifier: GPL-2.0-or-later

import os
import sys
import unittest

sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)))

if 'GDP_UNINSTALLED' not in os.environ:
    sys.path.insert(0, '/usr/share/game-data-packager')
    sys.path.insert(0, '/usr/share/games/game-data-packager')

from game_data_packager.data import (PackageRelation)  # noqa: E402
from game_data_packager.packaging.deb import (DebPackaging)  # noqa: E402


class DebTestCase(unittest.TestCase):
    def setUp(self) -> None:
        pass

    def test_rename_package(self) -> None:
        dp = DebPackaging()

        def t(in_: str, out: str) -> None:
            self.assertEqual(dp.rename_package(in_), out)

        # generic
        t('libc.so.6', 'libc6')
        t('libalut.so.0', 'libalut0')
        t('libXxf86dga.so.1', 'libxxf86dga1')
        t('libopenal.so.1', 'libopenal1')
        t('libstdc++.so.6', 'libstdc++6')
        t('libdbus-1.so.3', 'libdbus-1-3')

        # special cases
        t('libSDL-1.2.so.0', 'libsdl1.2debian')
        t('libgcc_s.so.1', 'libgcc1')
        t('libjpeg.so.62', 'libjpeg62-turbo | libjpeg62')

    def test_relation(self) -> None:
        dp = DebPackaging()

        def t(
            in_: list[str | dict[str, str]],
            out: list[str]
        ) -> None:
            self.assertEqual(
                    sorted(dp.format_relations(map(PackageRelation, in_))),
                    out)

        t(['libc.so.6'], ['libc6'])
        t(['libc.so.6 (>= 2.19)'], ['libc6 (>= 2.19)'])
        t(['libjpeg.so.62'], ['libjpeg62-turbo | libjpeg62'])
        t(['libopenal.so.1 | bundled-openal'], ['libopenal1 | bundled-openal'])
        t(['libc.so.6', 'libopenal.so.1'], ['libc6', 'libopenal1'])
        t([dict(deb='foo', rpm='bar')], ['foo'])
        t([dict(deb='foo', rpm='bar', generic='baz')], ['foo'])
        t([dict(rpm='bar', generic='baz')], ['baz'])
        t([dict(rpm='bar')], [])

    def tearDown(self) -> None:
        pass


if __name__ == '__main__':
    from gdp_test_common import main
    main()
