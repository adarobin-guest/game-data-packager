# encoding=utf-8
# Copyright 2016 Simon McVittie
# SPDX-License-Identifier: FSFAP

# This version of this file is only used when run uninstalled. It is replaced
# with a generated version during installation.
from game_data_packager.version import (
        GAME_PACKAGE_RELEASE,
        GAME_PACKAGE_VERSION,
        )

# reassure pyflakes
GAME_PACKAGE_RELEASE
GAME_PACKAGE_VERSION

__all__ = ['GAME_PACKAGE_RELEASE', 'GAME_PACKAGE_VERSION']
