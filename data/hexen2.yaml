---
# Copyright 2012-2013 Jonathan Dowland
# Copyright 2015-2017 Alexandre Detiste
# Copyright 2015-2018 Simon McVittie
# SPDX-License-Identifier: GPL-2.0-only

longname: Hexen II
franchise: Heretic
copyright: "\u00a9 1997 Raven Software, id Software, Activision"
engine: "uhexen2 | hexen2-engine"
genre: First-person shooter
wikipedia: https://en.wikipedia.org/wiki/Hexen_II

help_text: |
  The hexen2-data package requires data1/pak0.pak and data1/pak1.pak from
  version 1.11 (patched) or version 1.03 (retail CD-ROM).

  For the mission pack, Portal of Praevus, you must also provide
  portals/pak3.pak.

  A demo is available here, but cannot be downloaded noninteractively:
  https://www.fileplanet.com/10770/download/Hexen-II---Demo

packages:
  hexen2-demo-data:
    demo_for: hexen2-data
    mutually_exclusive: True
    install_to: $assets/hexen2
    install:
    - data1/pak0.pak?demo
    license:
    - HEXEN II SUBLICENSE.doc
    # Word 6 .doc, can be translated with antiword

  hexen2-data:
    steam:
      id: 9060
      path: "common/Hexen 2"
    install:
    - data1/pak0.pak?v1.11
    - data1/pak1.pak?v1.11

  hexen2-portals-data:
    longname: "Hexen II Mission Pack: Portal of Praevus"
    expansion_for: hexen2-data
    install_to: $assets/hexen2
    install:
    - portals/pak3.pak

  hexen2-hexenworld-data:
    longname: "Hexen II: HexenWorld"
    expansion_for: hexen2-data
    install_to: $assets/hexen2
    install:
    - hw/pak4.pak
    activated_by:
    - hw/pak4.pak
    - hexenworld-pakfiles-0.15.tgz

files:
  hexenworld-pakfiles-0.15.tgz:
    provides:
    - hw/pak4.pak
    download: http://downloads.sourceforge.net/project/uhexen2/Hexen2%20GameData/hexenworld-pakfiles/hexenworld-pakfiles-0.15.tgz?r=http%3A%2F%2Fuhexen2.sourceforge.net%2Fdownload.html
    unpack:
      format: tar.gz

  gamedata-all-1.29a.tgz:
    download: http://downloads.sourceforge.net/project/uhexen2/Hexen2%20GameData/gamedata-1.29a/gamedata-all-1.29a.tgz?r=http%3A%2F%2Fuhexen2.sourceforge.net%2Fdownload.html
    unpack:
      format: tar.gz
    provides:
      - patchdat/data1/data1pk0.xd3
      - patchdat/data1/data1pk1.xd3

  patchdat/data1/data1pk0.xd3:
    unpack:
      format: xdelta3
      other_parts:
        - data1/pak0.pak?v1.03
    provides:
      - data1/pak0.pak?v1.11

  patchdat/data1/data1pk1.xd3:
    unpack:
      format: xdelta3
      other_parts:
        - data1/pak1.pak?v1.03
    provides:
      - data1/pak1.pak?v1.11

  hexen2demo_nov1997-linux-i586.tgz?20181104:
    download: http://sourceforge.net/project/downloading.php?group_id=124987&filename=hexen2demo_nov1997-linux-i586.tgz
    provides:
      - data1/pak0.pak?demo
      - HEXEN II SUBLICENSE.doc
    unpack:
      format: tar.gz

  # Timestamps comes from here:
  # tar xzvfO hexen2demo_nov1997-linux-i586.tgz hexen2demo_nov1997/docs/CHANGES | head

  hexen2demo_nov1997-linux-i586.tgz?20160110:
    provides:
    - data1/pak0.pak?demo
    - HEXEN II SUBLICENSE.doc
    unpack:
      format: tar.gz

  hexen2demo_nov1997-linux-i586.tgz?20130315:
    unpack:
      format: tar.gz
    provides:
    - data1/pak0.pak?demo
    - HEXEN II SUBLICENSE.doc

  h2demoupd.exe:
    unpack:
      format: zip
    provides:
    - data1/pak0.pak?demo
    - HEXEN II SUBLICENSE.doc

  HEXEN II SUBLICENSE.doc:
    look_for: [HEXEN II SUBLICENSE.doc, SUBLICENSE.doc]
    install_as: hexen_II_sublicense.doc

size_and_md5: |
  21714275 b53c9391d16134cb3baddc1085f18683 data1/pak0.pak?v1.03
  22704056 c9675191e75dd25a3b9ed81ee7e05eff data1/pak0.pak?v1.11
  76958474 9a2010aafb9c0fe71c37d01292030270 data1/pak1.pak?v1.03
  75601170 c2ac5b0640773eed9ebe1cda2eca2ad0 data1/pak1.pak?v1.11
  49089114 77ae298dd0dcd16ab12f4a68067ff2c3 portals/pak3.pak
  10780245 88109ee385d9723ac5f1015e034a44dd hw/pak4.pak
  4948641  9ac598a80765daa0862893bd0aac765a hexenworld-pakfiles-0.15.tgz
  27750257 8e598d82bf53436ed7a0e133aa4b9f09 data1/pak0.pak?demo
  30208    f610eb93304c4cd2cdce97bdb9140e4e HEXEN II SUBLICENSE.doc
  15334912 d058ecd35e419a5b8bdf17003a006843 h2demoupd.exe
  13422252 a783d37b21863fb0190af234edb1c050 hexen2demo_nov1997-linux-i586.tgz?20181104
  19585198 b486f7b7a01b359ad801eac9486ba25f hexen2demo_nov1997-linux-i586.tgz?20130315
  13419798 7257d7b5f366c7bbf7328c6fb8160008 hexen2demo_nov1997-linux-i586.tgz?20160110
  3703846  4efd730fdd7d0496f320c40caa19b8b7 gamedata-all-1.29a.tgz
  1712606  651875f3aae22893050846f9b1cb4449 patchdat/data1/data1pk0.xd3
  1422024  3b7f2bb3ae64ac1cfa8ad7e3084efc8b patchdat/data1/data1pk1.xd3

sha1sums: |
  15358c105f88f8099fe889150a83022992c7357b  data1/pak0.pak?v1.03
  7c6b19d76ce85771026ea8dbdc397cd1109355b7  data1/pak0.pak?v1.11
  bd7d83349f8718ae5f16614ae99857b06a22fdd0  data1/pak1.pak?v1.03
  34c17f508bb6cd9ae4401307d0930a6f00761826  data1/pak1.pak?v1.11
  84aaba667060e1f9c50fb4d140fb301ac7e81cb7  portals/pak3.pak
  b21ecc51b15785b7d468dd353bce67cc13e7615a  hw/pak4.pak
  a7600b80b4205788b64fb57191e194c28b55b522  hexenworld-pakfiles-0.15.tgz
  a8cfd8790819c47998141adb97c14d1680cf8738  data1/pak0.pak?demo
  1aadeaf00dc72726e03bcfeb2fa0af93107852a7  HEXEN II SUBLICENSE.doc
  dcb5ab7c92352a8af4975151bd089354723d9417  h2demoupd.exe
  1633010f5c33afcf00931425d7d190eaffeed579  hexen2demo_nov1997-linux-i586.tgz?20130315
  7e850c6a28fe44a2cd4890fcab045486463bfcba  hexen2demo_nov1997-linux-i586.tgz?20181104
  75c6476fdb91ce8e56ca7e4f7f3068fc671477e0  gamedata-all-1.29a.tgz
  d14ee80d5c0224230888bd27f697ad59a78a441b  patchdat/data1/data1pk0.xd3
  e9cc009c8ac2b9795e7fcd3f60f794470e0c7f56  patchdat/data1/data1pk1.xd3

sha256sums: |
  57936acc4cd9abed7acb480dd9c6d59c7c98e96e4eca4e2d04f390d2dfa5ffde  data1/pak0.pak?v1.11
  6940cc03dc731f9c19f4e8a21259d7f191bd60a7487b50ceed3f8268e0d32a56  data1/pak1.pak?v1.11
  f5f53e360d49af9d7f8bd3b136a0ebab70e9ddcace13cf8aa5e182d986a0a48c  portals/pak3.pak
  140664a9a1c0b38d1dd9fc7cd1b9b051e0132d33ed5bf17117c1fc831eea0741  hw/pak4.pak
  49462cdf984deee7350d03c7d192d1c34d682647ffc9d06de4308e0a7c71c4d9  hexenworld-pakfiles-0.15.tgz
  0d4aa01a9909771dfa8e5be27db5d6628dc92f1406998c1a89c27d4748aaf151  data1/pak0.pak?demo
  b9841c4b9c2de220d393ab06650779782cdb905f4721367607abf1dd56316da4  HEXEN II SUBLICENSE.doc
  b349824ba09ddf0e95ca8a0f1783640d745c560b67da20a9cc31d1e816e63ab8  h2demoupd.exe
  f1f5ec95a33ca6a81eb27034b82b3c76d29004585baf6cfa2a1a21ab089b7ae1  hexen2demo_nov1997-linux-i586.tgz?20130315
  2df15cde0128b7a036e71995e068ca853f13be8e2b591caac140025d66643fc0  hexen2demo_nov1997-linux-i586.tgz?20181104
  f3a8e43d786176599c5b9b1651315f76a6d4391b181d694657796b6e2c6ed646  gamedata-all-1.29a.tgz
  deba9d41e95ad76e2d4623b99125f3fea01f48bd9fb55ff168caa717afa04189  patchdat/data1/data1pk0.xd3
  731e2c81541d246c6d7fc2e8972f1c021dfc75479ea70dc1b62f773d6ec42aa7  patchdat/data1/data1pk1.xd3

...
# vim:set sw=2 sts=2 et:
