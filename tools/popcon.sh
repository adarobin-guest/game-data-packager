#!/bin/bash
# Copyright 2016 Alexandre Detiste
# SPDX-License-Identifier: GPL-2.0-only

mkdir -p /var/cache/popcon/

date=$(date +%Y%m%d)
wget http://popcon.debian.org/by_inst.gz --no-clobber --output-document=/var/cache/popcon/debian_$date.gz

# $ curl http://popcon.ubuntu.com/
# Thanks !
