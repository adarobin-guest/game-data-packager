#!/usr/bin/python3
# encoding=utf-8
#
# Copyright © 2015 Alexandre Detiste <alexandre@detiste.be>
# SPDX-License-Identifier: GPL-2.0-or-later

# Usage: _build/run-tool-uninstalled tools/check_steam.py

import json
import os
import urllib.request

from game_data_packager.game import load_games
from game_data_packager.util import AGENT

url = 'https://github.com/SteamDatabase/SteamLinux/raw/master/GAMES.json'
response = urllib.request.urlopen(urllib.request.Request(url,
                                  headers={'User-Agent': AGENT}))
native = json.loads(response.read().decode('utf8'))
native = set(int(id) for id in native.keys())

for shortname, game in load_games(
    datadir=os.environ.get('GDP_BUILDDIR', 'out')
).items():
    for package in game.packages.values():
        steam = package.steam or game.steam
        if not steam:
            continue
        if steam.get('native') and steam.get('id') in native:
            print('correctly tagged as native: %s' % package.name)
        elif steam.get('native'):
            print('extraneously tagged as native: %s' % package.name)
        elif steam.get('id') in native:
            print('should be tagged as native: %s' % package.name)
