#!/usr/bin/python3
#
# Copyright © 2023 Alexandre Detiste <alexandre@detiste.be>
# SPDX-License-Identifier: GPL-2.0-or-later

# detect packages that are both generated by GDP
# and into official the archive

import glob
import os

import apt

srcdir = os.path.dirname(
    os.path.dirname(
        os.path.abspath(__file__)
    )
)


gdp: set[str] = set()

for yaml_file in sorted(glob.glob(os.path.join(srcdir, 'data', '*.yaml'))):
    with open(yaml_file, encoding='utf-8') as raw:
        for line in raw:
            if line.strip() == 'packages:':
                break
        for line in raw:
            if line == '\n':
                continue
            if line[0] != ' ':
                break
            if line[2] not in (' ', '#'):
                package = line.strip(':\n ')
                gdp.add(package)

archive: set[str] = set()

cache = apt.Cache()
for p in cache.keys():
    o = cache[p]
    if o.candidate and o.candidate.downloadable:
        archive.add(o.shortname)

print(sorted(archive & gdp))

# ['alex4-data', 'pushover-data']
# https://alioth-lists.debian.net/pipermail/pkg-games-devel/2023-December/063512.html
# https://alioth-lists.debian.net/pipermail/pkg-games-devel/2023-December/063510.html
