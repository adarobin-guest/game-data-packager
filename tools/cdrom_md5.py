#!/usr/bin/python3
# encoding=utf-8
#
# Copyright © 2016 Alexandre Detiste <alexandre@detiste.be>
# SPDX-License-Identifier: GPL-2.0-or-later

import hashlib
import sys

CDROM_SECTOR = 2048

if len(sys.argv) != 2:
    exit('Usage: cdrom_md5.py <file>')

with open(sys.argv[1], 'rb') as f:
    while True:
        blob = f.read(CDROM_SECTOR)
        if not blob:
            break
        md5 = hashlib.new('md5')
        md5.update(blob)
        print(md5.hexdigest())
