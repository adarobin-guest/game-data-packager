#!/usr/bin/python3
# encoding=utf-8
#
# Copyright © 2024 Alexandre Detiste <alexandre@detiste.be>
# SPDX-License-Identifier: GPL-2.0-or-later

import os
import subprocess

from game_data_packager.game import load_games
from game_data_packager.games.scummvm_common import (ScummvmGameData,
                                                     ScummvmPackage)


def get_scummvm_ids() -> set[str]:
    result = set()
    stdout = subprocess.check_output(['scummvm', '--list-all-games'],
                                     text=True)
    for line in stdout.splitlines():
        if ':' in line:
            gameid = line.split()[0]
            result.add(gameid)
    return result


scummvm_ids = get_scummvm_ids()

scummvm_ids2: dict[str, int] = dict()
for gameid in scummvm_ids:
    assert gameid not in scummvm_ids2
    scummvm_ids2[gameid] = 1

    oldgameid = gameid.split(':')[1]
    scummvm_ids2[oldgameid] = scummvm_ids2.get(oldgameid, 0) + 1

scummvm_ids3: set[str] = set()
for gameid, count in scummvm_ids2.items():
    if count > 1:
        # print('discarding non-discriminating id: %s' % gameid)
        pass
    else:
        scummvm_ids3.add(gameid)


def get_gdp_ids() -> set[str]:
    games = set()
    for name, game in load_games(
        datadir=os.environ.get('GDP_BUILDDIR', 'out')
    ).items():
        if game.wikibase != 'https://wiki.scummvm.org/index.php/':
            continue
        assert isinstance(game, ScummvmGameData), game
        game.load_file_data()
        for package in game.packages.values():
            assert isinstance(package, ScummvmPackage), package
            gameid = package.gameid or game.gameid
            games.add(gameid)
    return games


gdp_ids = get_gdp_ids()
# print(gdp_ids)

print("Unknown game id's:")
print(sorted(gdp_ids - scummvm_ids3))
